variable "region" {
  default = "us-east-1"
}

variable "ami_id" {
  type = "map"
  default = {
    us-east-1    = "ami-042e8287309f5df03"
    eu-west-2    = "ami-096fda3c22c1c990a"
  }
}

variable "instance_type" {
  type    = "string"
  default = "t2.micro"
}
variable "key_name" {
  type    = "string"
  default = "WindowsKey"
}