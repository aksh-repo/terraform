resource "aws_instance" "web1" {
   ami           = "${lookup(var.ami_id, var.region)}"
   instance_type = "${var.instance_type}"

 # key name
  key_name = "${var.key_name}"
  
  # Security group assign to instance
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  
  user_data = <<EOF
		         sudo apt-get update
		         sudo apt-get install software-properties-common
		         sudo apt-add-repository --yes --update ppa:ansible/ansible
		         sudo apt-get install ansible
                 
                 EOF  
  
  tags = {
    Name = "Ubuntu Terraform"
  }
 }